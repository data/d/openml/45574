# OpenML dataset: web_questions

https://www.openml.org/d/45574

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset consists of 6,642 question/answer pairs. The questions are supposed to be answerable by Freebase, a large knowledge graph. The questions are mostly centered around a single named entity. The questions are popular ones asked on the web (at least in 2013). Taken from https://huggingface.co/datasets/web_questions.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45574) of an [OpenML dataset](https://www.openml.org/d/45574). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45574/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45574/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45574/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

